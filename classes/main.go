package main

import "fmt"

// Equivalencia a Clases
type Employee struct {
	id int
	name string
}

// los metodos de la clase mediante recievers functions
func (e *Employee) SetId(id int) {
	e.id = id
}

func (e *Employee) SetName(name string) {
	e.name = name
}

func (e *Employee) GetId() int {
	return e.id
}

func (e *Employee) GetName() string {
	return e.name
}

// otra forma de realizar el constructor
func NewEmployee (id int, name string) *Employee {
	return &Employee{
		id: id,
		name: name,
	}
}

func main() {
	emple := Employee{}
	fmt.Printf("%v\n", emple)
	emple.id = 1
	emple.name = "Jean Pierre"
	fmt.Printf("%v\n", emple)
	emple.SetId(2)
	emple.SetName("Giovanni")
	fmt.Println(emple.GetId())
	fmt.Println(emple.GetName())

	emple2 := Employee {
		id: 3,
		name: "Gustavo",
	}

	fmt.Println(emple2)

	emple3 := new(Employee)
	fmt.Printf("%v\n",*emple3)

	emple4 := NewEmployee(6,"Claudia")
	fmt.Printf("%v", emple4)
}