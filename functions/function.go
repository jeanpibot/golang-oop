package main

import "fmt"

//  Esta seccion es para explicar las funciones anónimas las cuales se usan cuando solo se necesitan una vez
//  es importante que solo se debe usar una vez y que estemos seguros de no volverla a implementar 


// ahora para las funciones que tenemos y vamos agregando muchos valores pero no sabemos cuantos nos va traer
// usamos las funciones variadicas

// Function variadicas
func sum(valores ...int) int {
	total := 0
	for _, valor := range valores {
		total += valor
	}
	return total
}

// functiones que retornos con nombre
func getValues(x int) (double, triple, quad int) {
	double = 2 * x
	triple = 3 * x
	quad = 4 * x
	// no es necesario escribir cada uno de los valores doble, triple y quad
	return 
}

func main() {
	x := 5
	// Esta es una funcion anónima
	y := func() int {
		return x * 2
	}()
	fmt.Println(y)
	
	fmt.Println(sum(x, 2, 4))

	fmt.Println(getValues(2))
}
