package main

import "testing"

func TestSum(t *testing.T) {
	// se utilizan tablas para hacer diferentes clases de test en uno solo
	tables := []struct {
		a int
		b int
		n int
	}{
		{1,2,3},
		{2,2,4},
		{25, 26, 51},
	}

	for _, item := range tables {
		total := Sum(item.a, item.b)
		if total != item.n {
			t.Errorf("Sum was incorrect, got %d, expected %d", total, item)
		}
	}	
}

func TestGetMax(t *testing.T) {
	tables := []struct {
		a int
		b int
		c int
	} {
		{4, 2, 4},
		{3, 2, 3},
		{5, 3, 5},
		{2, 5, 5},
		{8, 20, 20},
	}

	for _, item := range tables {
		max := GetMax(item.a, item.b)

		if max != item.c {
			t.Errorf("Get Max was incorrect, got %d expected %d", max, item.c)
		}
	}
}

func TestFibonnaci(t *testing.T) {
	tables := []struct {
		a int
		b int
	} {
		{1, 1},
		{8, 21},
		{50, 12586269025},
	}

	for _, item := range tables {
		fibon := Fibonnaci(item.a)

		if fibon != item.b {
			t.Errorf("Fibonnaci was incorrect, got %d expected %d", fibon, item.b)
		}
	}
}